from dizzer import decode, encode
from dizzer._codec import _Cache, _get_scrambling_sequence, _restoring_random_number_generator_state, \
    _scramble_word, _split_text_parts


def test_encode():
    assert encode('') == ''
    assert encode("This machine encodes\n that looong text.") == 'Tihs mhcaine eoecnds\n taht loonog txet.'
    assert decode("Tihs mhcaine eoecnds\n taht loonog txet.") == "This machine encodes\n that looong text."

    assert encode("Text can contain unlimited number of whitespaces, newlines etc.") == \
           "Txet can cnitaon umnitelid nmeubr of wtsehaecips, newniels etc."

    assert encode("""
Text can contain unlimited number of whitespaces, newlines etc.
Numbers like 12345, 1234567, 987654321 or 123456789 are also scrambled.
""") == """
Txet can cnitaon umnitelid nmeubr of wtsehaecips, newniels etc.
Numbres lkie 14325, 1346257, 967843251 or 143267859 are aslo scrmeabld.
"""

    # clear sequences - just for a sanity check
    _Cache.sequences = {}

    assert decode("""
Txet can cnitaon umnitelid nmeubr of wtsehaecips, newniels etc.
Numbres lkie 14325, 1346257, 967843251 or 143267859 are aslo scrmeabld.
""") == """
Text can contain unlimited number of whitespaces, newlines etc.
Numbers like 12345, 1234567, 987654321 or 123456789 are also scrambled.
"""


def test_scrambler():
    with _restoring_random_number_generator_state():
        assert _scramble_word('') == ''
        assert _scramble_word('by') == 'by'
        assert _scramble_word('one') == 'one'
        assert _scramble_word('four') == 'fuor'
        assert _scramble_word('abcd') == 'acbd'
        assert _scramble_word('abcdefg') == 'afcdbeg'
        assert _scramble_word('thing') == 'thnig'
        assert _scramble_word('permutation') == 'pmutitreoan'


def test_splitter():
    def t(text):
        # a shortcut - less typing, less reading
        return list(_split_text_parts(text))

    assert t('') == []
    assert t(' ') == [(' ', False)]
    assert t('  ') == [('  ', False)]
    assert t('a') == [('a', True)]
    assert t('abcd') == [('abcd', True)]
    assert t('abcd\t') == [('abcd', True), ('\t', False)]
    assert t('\t \tab\n cd') == [
        ('\t \t', False), ('ab', True), ('\n ', False), ('cd', True)]
    assert t('one,2,\tthree=12-6') == [
        ('one', True), (',', False), ('2', True), (',\t', False),
        ('three', True), ('=', False), ('12', True), ('-', False), ('6', True)]


def test_get_seed():
    assert _get_scrambling_sequence('some') == [1, 0]
    assert _get_scrambling_sequence('test') == [1, 0]
    assert _get_scrambling_sequence('words') == [0, 2, 1]
    assert _get_scrambling_sequence('testing') == [3, 0, 2, 4, 1]
    assert _get_scrambling_sequence('ipsum') == [2, 1, 0]
    assert _get_scrambling_sequence('iuspm') == [2, 1, 0]
