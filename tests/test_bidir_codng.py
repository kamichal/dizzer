import dizzer
from dizzer._codec import _Cache

ORIGINAL_TEXT = """
https://en.wikipedia.org/wiki/Lorem_ipsum

In publishing and graphic design, Lorem ipsum is a placeholder text
commonly used to demonstrate the visual form of a document or a typeface
without relying on meaningful content.

Lorem ipsum may be used before final copy is available, but it may
also be used to temporarily replace copy in a process called
greeking, which allows designers to consider form without the meaning
of the text influencing the design.

Lorem ipsum is typically a corrupted version of De finibus
bonorum et malorum, a first-century BC text by the Roman statesman
and philosopher Cicero, with words altered, added, and removed to make
it nonsensical, improper Latin.

Versions of the Lorem ipsum text have been used in typesetting at
least since the 1960s, when it was popularized by advertisements
for Letraset transfer sheets. Lorem ipsum was introduced to the digital
world in the mid-1980s when Aldus employed it in graphic and word-processing
templates for its desktop publishing program PageMaker.

Other popular word processors including Pages and Microsoft Word have since
adopted Lorem ipsum as well.

Source text

The Lorem ipsum text is derived from sections 1.10.32 and 1.10.33 of
Cicero's De finibus bonorum et malorum.[1][2] The physical source may have
been the 1914 Loeb Classical Library edition of De finibus, where the Latin text,
presented on the left-hand (even) pages, breaks off on page 34 with "Neque porro
quisquam est qui do- " and continues on page 36 with "lorem ipsum ... ", suggesting
that the galley type of that page was mixed up to make the dummy text seen today.[3]

The discovery of the text's origin is attributed to Richard McClintock, a Latin
scholar at Hampden–Sydney College. McClintock connected Lorem ipsum to Cicero's
writing sometime before 1982 while searching for instances of the Latin word
consectetur, which was rarely used in classical literature.[4] McClintock first
published his discovery in a 1994 letter to the editor of Before & After magazine,
contesting the editor's earlier claim that Lorem ipsum had no meaning.[5]

"""

ENCODED_TEXT = """
htpts://en.wdkieipia.org/wkii/Loerm_iuspm

In pibhsiunlg and girhapc dsiegn, Loerm iuspm is a pdcaelleohr txet
clmnoomy uesd to dretsaontme the vuiasl from of a docmnuet or a tcpaeyfe
whituot relinyg on mfugneainl conetnt.

Loerm iuspm may be uesd brfeoe fanil cpoy is aablaivle, but it may
aslo be uesd to triamoelpry repalce cpoy in a pcsroes celald
giekenrg, wchih aolwls desgrines to cnioesdr from whituot the mneaing
of the txet innlnicfueg the dsiegn.

Loerm iuspm is tyacilply a crproetud vseiorn of De finbuis
buonrom et mluoarm, a frsit-cutenry BC txet by the Raomn statemasn
and phliohpeosr Ceirco, wtih wodrs arleetd, adedd, and rmveeod to mkae
it nasnnecsoil, iormpepr Liatn.

Voersins of the Loerm iuspm txet hvae been uesd in teettiynpsg at
lseat scnie the 1690s, wehn it was prelpiouazd by aedmnetsrtives
for Leteasrt tnrsefar seeths. Loerm iuspm was inurecdotd to the diiagtl
wolrd in the mid-1890s wehn Aluds eymploed it in girhapc and wrod-peossirncg
teaetmlps for its dkestop pibhsiunlg poarrgm PaeMgkaer.

Oehtr ppalour wrod prooercsss ilcinundg Paegs and Mfoosirct Wrod hvae scnie
atdpeod Loerm iuspm as wlel.

Socure txet

The Loerm iuspm txet is derievd form steinocs 1.10.32 and 1.10.33 of
Ceirco's De finbuis buonrom et mluoarm.[1][2] The paychisl socure may hvae
been the 1194 Leob Cslaaiscl Lairrby etidion of De finbuis, wrhee the Liatn txet,
peertensd on the lfet-hnad (eevn) pgaes, bakres off on pgae 34 wtih "Nqeue proro
qaisquum est qui do- " and cteinnous on pgae 36 wtih "loerm iuspm ... ", sesgtngiug
taht the glealy tpye of taht pgae was mexid up to mkae the dmumy txet seen tdoay.[3]

The descivroy of the txet's oigirn is aiuttrbted to Rahircd MCnclitock, a Liatn
solahcr at Hdpaemn–Seydny Celolge. MCnclitock cncnoeted Loerm iuspm to Ceirco's
wniritg somtmeie brfeoe 1892 wlhie scireanhg for inscaents of the Liatn wrod
cnetocutesr, wchih was rarley uesd in caacsilsl ltaierture.[4] MCnclitock frsit
pbiuhesld his descivroy in a 1994 ltteer to the eiodtr of Bofree & Atfer maganize,
cisontnetg the eiodtr's eariler cialm taht Loerm iuspm had no mneaing.[5]

"""


def test_lorem_ipsum():
    encoded = dizzer.encode(ORIGINAL_TEXT)
    assert encoded == ENCODED_TEXT

    words_failed = sorted(set(
        org for org, crypt in zip(ORIGINAL_TEXT.split(), encoded.split())
        if org == crypt and len(org) > 3
    ))

    assert words_failed == ['1.10.32', '1.10.33', '1994', 'been', 'seen']

    # clear sequences - just for a sanity check
    _Cache.sequences = {}

    assert dizzer.decode(ENCODED_TEXT) == ORIGINAL_TEXT


def test_with_numbers():
    """Since it encodes numbers in the same way as words, let's check it.
        We get numbers with at least 4 digits.
    """

    num_failed = 0
    num_tested = 0

    for number in range(1000, 101000):
        encoded = dizzer.encode(str(number))
        decoded = int(dizzer.decode(encoded))

        assert decoded == number, "Cannot recover the original message."

        if encoded == str(decoded):
            if len(set(encoded[1:-1])) == len(encoded) - 2:
                if encoded[1] != encoded[-2]:
                    # skip cases like: 2002, 2449, 3880
                    # failed_to_encode.append(number)
                    num_failed += 1
        num_tested += 1

    assert not num_failed, f"\n {num_failed / num_tested * 100:.2f}% not encoded"
